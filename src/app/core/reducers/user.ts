
import { UserActionTypes, UserActions } from '../actions/user';

export interface UserState {
    authenticated: boolean;
    token: string;
}

const initialState: UserState = {
    authenticated: false,
    token: ""
};

export function reducer(
    state: UserState = initialState,
    action: UserActions
): UserState {
    switch (action.type) {
        case UserActionTypes.Login:
            return {
                authenticated: true,
                token: action.payload
            };

        case UserActionTypes.Logout:
            return {
                authenticated: false,
                token: ""
            };
        default:
            return state;
    }
}

export const isAuthenticated = (state: UserState) => state.authenticated;