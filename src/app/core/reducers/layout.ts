import { LayoutActionTypes, LayoutActions } from '../actions/layout';

export interface LayoutState {
  showSidenav: boolean;
}

const initialLayoutState: LayoutState = {
  showSidenav: false,
};

export function reducer(
  state: LayoutState = initialLayoutState,
  action: LayoutActions
): LayoutState {
  switch (action.type) {
    case LayoutActionTypes.CloseSidenav:
      return {
        showSidenav: false,
      };

    case LayoutActionTypes.OpenSidenav:
      return {
        showSidenav: true,
      };

    default:
      return state;
  }
}

export const getShowSidenav = (state: LayoutState) => state.showSidenav;