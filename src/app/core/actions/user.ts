import { Action } from '@ngrx/store';

export enum UserActionTypes {
    Login = '[User] Login',
    Logout = '[User] Logout',
}

export class Login implements Action {
    readonly type = UserActionTypes.Login;
    constructor(public payload: string) { }
}

export class Logout implements Action {
    readonly type = UserActionTypes.Logout;
}

export type UserActions = Login | Logout;
