import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { InternalStorage, SDKStorage } from './storage/storage.swaps';
import { CookieBrowser } from './storage/cookie.browser';
import { CookieNode } from './storage/cookie.node';
import { SocketBrowser } from './sockets/socket.browser';
import { SocketNode } from './sockets/socket.node';
import { SocketDriver } from './sockets/socket.driver';
import { StorageBrowser } from './storage/storage.browser';
import { SocketConnection } from './sockets/socket.connections';


import { SERVICES } from './services';

/**
* @module CoreModule
* @description
* This module should be imported when building a Web Application in the following scenarios:
*
*  1.- Regular web application
*  2.- Angular universal application (Browser Portion)
*  3.- Progressive applications (Angular Mobile, Ionic, WebViews, etc)
**/
@NgModule({
    imports: [CommonModule, HttpModule],
    declarations: [],
    exports: [],
    providers: [
        SocketConnection
    ]
})
export class CoreBrowserModule {
    static forRoot(internalStorageProvider: any = {
        provide: InternalStorage,
        useClass: CookieBrowser
    }): ModuleWithProviders {
        return {
            ngModule: CoreBrowserModule,
            providers: [
                ...SERVICES,
                internalStorageProvider,
                { provide: SDKStorage, useClass: StorageBrowser },
                { provide: SocketDriver, useClass: SocketBrowser }
            ]
        };
    }
}
/**
* @module SDKNodeModule
* @description
* This module should be imported when building a Angular Universal Application.
**/
@NgModule({
    imports: [CommonModule, HttpModule],
    declarations: [],
    exports: [],
    providers: [
        SocketConnection
    ]
})
export class CoreNodeModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: CoreNodeModule,
            providers: [
                ...SERVICES,
                { provide: InternalStorage, useClass: CookieNode },
                { provide: SocketDriver, useClass: SocketNode }
            ]
        };
    }
}