/* tslint:disable */
export * from './auth.service';
export * from './error.service';
export * from './searchParams.service';
export * from './dataAccessBase.service';
export * from './realTime.service';

import { LoopBackAuth } from './auth.service';
import { ErrorHandler } from './error.service';
import { JSONSearchParams } from './searchParams.service';
import { RealTime } from './realTime.service';

import { DOMAIN_SERVICES } from './custom';
export * from './custom';

export const CORE_SERVICES = [
    LoopBackAuth,
    ErrorHandler,
    JSONSearchParams,
    RealTime
]

export const SERVICES = [
    ...DOMAIN_SERVICES,
    ...CORE_SERVICES
]