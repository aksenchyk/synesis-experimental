/* tslint:disable */
export * from './user.service';
export * from './organization.service';

import { UserApi } from './user.service';
import { OrganizationApi } from './organization.service';

export const DOMAIN_SERVICES = [
    UserApi,
    OrganizationApi 
]