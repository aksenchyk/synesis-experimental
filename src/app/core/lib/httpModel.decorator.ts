export function httpModel<T>(path: string): any {
    return (Target: any, propertyKey: string, descriptor: TypedPropertyDescriptor<any>) => {
        Target.factory = function (data): T {
            return new Target(data);
        }
        Target.getPath = function (): string {
            return path;
        }
        return Target;
    };
}
