import { PersistentEntityBase } from './PersistentEntityBase';
import { httpModel } from '../core/lib';

export interface ICustomer {
  firstName: string;
  lastName: string;
  id?: number;
  organizationId?: number;
}

@httpModel('customers')
export class Customer extends PersistentEntityBase implements ICustomer {
  firstName: string;
  lastName: string;
  id: number;
  organizationId: number;
  constructor(instance?: ICustomer) {
    super();
    Object.assign(this, instance);
  }
}
