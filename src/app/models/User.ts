/* tslint:disable */
import { PersistentEntityBase } from './PersistentEntityBase';
import { httpModel } from '../core/lib';

export interface UserInterface {
  "realm"?: string;
  "username"?: string;
  "email": string;
  "emailVerified"?: boolean;
  "id"?: number;
  "password"?: string;
  accessTokens?: any[];
}

@httpModel('users')
export class User extends PersistentEntityBase implements UserInterface {
  "realm": string;
  "username": string;
  "email": string;
  "emailVerified": boolean;
  "id": number;
  "password": string;
  accessTokens: any[];
  constructor(data?: UserInterface) {
    super();
    Object.assign(this, data);
  }
}