/* tslint:disable */
import { Customer } from './Customer';
import { PersistentEntityBase } from './PersistentEntityBase';
import { httpModel } from '../core/lib';

export interface IOrganization {
  name: string;
  address: any;
  id?: number;
  customers?: Customer[];
}

@httpModel('organizations')
export class Organization extends PersistentEntityBase implements IOrganization {
  name: string;
  address: any;
  id: number;
  customers: Customer[];
  constructor(instance?: IOrganization) {
    super();
    this.customers = [];
    Object.assign(this, instance);
  }
}

