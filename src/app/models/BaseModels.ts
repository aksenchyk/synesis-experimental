/* tslint:disable */



declare var Object: any;
export interface LoopBackFilter {
  fields?: any;
  include?: any;
  limit?: any;
  order?: any;
  skip?: any;
  offset?: any;
  where?: any;
}

export interface AccessTokenInterface {
  "id"?: string;
  "ttl"?: number;
  "scopes"?: ["string"];
  "created"?: Date;
  "userId"?: string;
  "user"?: any;
}

export class AccessToken implements AccessTokenInterface {
  "id": string;
  "ttl": number;
  "scopes": ["string"];
  "created": Date;
  "userId": string;
  "user": any;
  constructor(data?: AccessTokenInterface) {
    Object.assign(this, data);
  }
}
export class SDKToken implements AccessTokenInterface {
  id: any = null;
  ttl: number = null;
  scopes: any = null;
  created: any = null;
  userId: any = null;
  user: any = null;
  rememberMe: boolean = null;
  constructor(data?: AccessTokenInterface) {
    Object.assign(this, data);
  }
}
/**
 * This GeoPoint represents both, LoopBack and MongoDB GeoPoint
 **/
export interface GeoPoint {
  lat?: number;
  lng?: number;
  type?: string;
  coordinates?: number[];
}

/**
 * Google api compatible address
 **/
export interface Address {
  city?: number;
  zip?: number;
  state?: string;
  place?: GeoPoint;
  formatted_address?: string
}
