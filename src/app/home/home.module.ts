import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HomeComponent } from './home.component';

import { HomeRouting } from './home.routing';


@NgModule({
    imports: [
        HomeRouting,
        //  SharedModule
    ],
    declarations: [
        HomeComponent
    ],
    providers: [
        //  HomeAuthResolver
    ]
})
export class HomeModule { }