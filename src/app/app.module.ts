import { NgModule, ApplicationRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
//import { NgUploaderModule } from 'ngx-uploader';

import { AppComponent } from './app.component';

// import { AUTHENTICATION_COMPONENTS } from './auth';
// import { NotFoundComponent } from './notFound';

import { CoreBrowserModule } from './core';
import { AppRoutingModule } from './app.routing.module';
//import { DomainModule } from './domain/domain.module'

import { HomeModule } from './home/home.module';
//import { SharedModule } from "./shared";
// import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// // Redux
import { StoreModule, INITIAL_STATE } from '@ngrx/store';
import { reducers, metaReducers } from './core/reducers';

// main styles
//import '../theme/styles.scss';

const getInitialState = () => {
  console.log("--------------------------- Setting initial state...")
  return {
    user: {
      authenticated: false,
      token: ""
    },
    layout: {
      showSidenav: false // or read from cookies
    }
  };
}

import {
  LocationStrategy,
  // HashLocationStrategy
} from '@angular/common';

@NgModule({
  bootstrap: [AppComponent],
  declarations: [
    AppComponent,
    //  ...AUTHENTICATION_COMPONENTS,
    //  NotFoundComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    //  SharedModule.forRoot(),
    //  NgbModule.forRoot(),
    //  StoreModule.provideStore(reducer),
    CoreBrowserModule.forRoot(),
    StoreModule.forRoot(reducers, { metaReducers }),
    //DomainModule,
    AppRoutingModule,
    HomeModule
    //BrowserAnimationsModule
  ],

  providers: [
    //  { provide: LocationStrategy, useClass: HashLocationStrategy },


    // in the providers array
    { provide: INITIAL_STATE, useFactory: getInitialState }
  ]
})
export class AppModule {
  constructor(public appRef: ApplicationRef) { }
}

/**
 * TODO LIST:
 * - see https://github.com/mpalourdio/ng-http-loader for spinner solution using ng4+ interceptors
 */