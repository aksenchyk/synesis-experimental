//import { AuthenticationRoutes } from './auth';

import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules, NoPreloading } from '@angular/router';
//import { NotFoundComponent } from './notFound';

export const routes: Routes = [
 //   { path: '', component: HomePageComponent, pathMatch: 'full' },
    //  { path: '', component: NavbarComponent, outlet: 'navbar' },
    //  { path: '', component: HomeComponent },
    //  { path: 'customers', loadChildren: './domain/+customers/customers.module#EntriesModule' },
    //  { path: 'services', loadChildren: './+services/services.module#ServicesModule' },
      { path: '', loadChildren: './domain/domain.module#DomainModule' },
    //  ...AuthenticationRoutes,

    // { path: '**', component: NotFoundComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }