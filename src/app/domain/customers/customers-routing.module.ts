import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CustomerBaseComponent } from './customers-base';
import { CustomerListComponent } from './customers-list';

// import { IsAuthenticatedGuard } from '../core';
// import { StatusResolver } from './shared'

export const routes: Routes = [
  {
    path: '',
    component: CustomerBaseComponent,
    //  canActivate: [IsAuthenticatedGuard],
    children: [
      {
        path: '',
        component: CustomerListComponent
      }
    ]
  },

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomersRoutingModule { }
