import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomersRoutingModule } from './customers-routing.module';
import { CustomerDomainService } from "./services"
import { CustomerBaseComponent } from './customers-base';
import { CustomerListComponent } from './customers-list'

@NgModule({
  imports: [
    CommonModule,
    CustomersRoutingModule
  ],
  declarations: [
    CustomerBaseComponent,
    CustomerListComponent
  ],
  providers: [
    CustomerDomainService,
  ]
})
export class CustomersModule { }
