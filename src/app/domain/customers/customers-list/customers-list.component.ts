import { Component, ViewContainerRef } from '@angular/core';
import { LoopBackAuth, RealTime } from "../../../core";
import { Store } from '@ngrx/store';
import { State } from '../../../core/reducers';
import * as userActions from '../../../core/actions';
import { Customer } from "../../../models";
import { CustomerDomainService } from "../services";


@Component({
    template:
        `
    <div>You-ho-ho</div>
    `
})
export class CustomerListComponent {

    constructor(
        public authService: LoopBackAuth,
        private store: Store<State>,
        private _cs: CustomerDomainService,
        public viewContainerRef: ViewContainerRef) {

    }

    ngOnInit() {

        setInterval(() => {
            this.store.dispatch(new userActions.Login("someToken"))
            this._cs
                .create(new Customer({ firstName: "Bishop", lastName: "Tompson" }))
                .subscribe((result) => {
                    console.log(result);
                }, (err) => {
                    console.log(err);
                });
        }, 2000)

        this._cs.onCreate([{}])
            .subscribe((res) => {
                console.log('SOCKET RESULT', res)
            }, err => {
                console.log('SOCKET ERR', err)
            })
    }
}