import { NgModule, ApplicationRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';


import { DomainRouting } from './domain.routing';
import { CustomersModule } from "./customers/customers.module"

import { DomainsBaseComponent } from './domain.component';


@NgModule({
    declarations: [
        DomainsBaseComponent
    ],
    imports: [
        CommonModule,
        DomainRouting,
        CustomersModule
    ],
    providers: [
    ]
})

export class DomainModule {
    constructor() { } 
}