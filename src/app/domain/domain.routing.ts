import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DomainsBaseComponent } from './domain.component';

const routes: Routes = [
    {
        path: '',
        component: DomainsBaseComponent,
        children: [
            {
                path: 'customers',
                loadChildren: './customers/customers.module#CustomersModule'
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class DomainRouting { }