import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { CoreConfig, LoopBackAuth, RealTime } from './core';
import { Store } from '@ngrx/store';
import { State } from './core/reducers';
import * as userActions from './core/actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(
    public authService: LoopBackAuth,
    private store: Store<State>,   
    private realTime: RealTime,
    public viewContainerRef: ViewContainerRef) {
    CoreConfig.setBaseURL('');
    //  this.appController.start();
  }

  ngOnInit() {
    this.realTime
      .onReady()
      .subscribe(() => {
        console.log('NgApp has been connected');
      });
    this.realTime.onDisconnect().subscribe((error: string) => {
      console.log('Disconnected', error);
    });
    this.realTime.onUnAuthorized().subscribe((error: string) => {
      // this.router.navigate(['/access-ngrx']);
    });
    this.realTime.onAuthenticated().subscribe((error: string) => {
      console.log('I am authorized');
    });
  }

  // getLoaded() {
  //   return this._store
  //     .let(getLoaded())
  //     .map(value => !value);
  // }
}
